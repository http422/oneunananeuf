$(document).ready(function(){

//-------------------------------------------------------------------------
//start functions for "one"
	
	//on mouse over
	$( ".one" ).mouseover(function() {
		$("#one_o").css('color', 'red');
		$("#one_n").css('color', 'red');
		$("#neun_e").css('color', 'red');
		$( ".one" ).css('cursor','pointer');
		
		//moving tag
		$(".one").on('mousemove', function(e){
			$('.yi_tag').css({
				opacity:1,
				left:  e.pageX,
				top:   e.pageY
			});
		});
	});
	
	//on mouse out
	$( ".one" ).mouseout(function() {
		$("#one_o").css('color', 'black');
		$("#one_n").css('color', 'black');
		$("#neun_e").css('color', 'black');
		
		
		//disappear
		$('.yi_tag').css({
			opacity:0
		});	
	});

	//on click, redirect to the page for english
	$( ".one" ).click(function(){
		document.location = 'http://earthling.blog/';
		// if storing in own directory, use document.location = 'french/french.html';
	});
	
//-------------------------------------------------------------------------
//start functions for "neun"

	$( ".neun" ).mouseover(function() {
		$("#one_n").css('color', 'red');
		$("#neun_e").css('color', 'red');
		$("#neun_u").css('color', 'red');
		$("#neun_n").css('color', 'red');
		$( ".neun" ).css('cursor','pointer');
		
		//moving tag
		$(".neun").on('mousemove', function(e){
			$('.jiu_tag').css({
				opacity:1,
				left:  e.pageX,
				top:   e.pageY
			});
		});
	});
	
	$( ".neun" ).mouseout(function() {
		$("#one_n").css('color', 'black');
		$("#neun_e").css('color', 'black');
		$("#neun_u").css('color', 'black');
		$("#neun_n").css('color', 'black');
		
		//disappear
		$('.jiu_tag').css({
			opacity:0
		});
	});
	
	//on click, redirect to the page for german
	$( ".neun" ).click(function(){
		document.location = 'http://earthling.blog/';
		// if storing in own directory, use document.location = 'french/french.html';
	});
	
//-------------------------------------------------------------------------
//start functions for "nana"
	
	$( ".nana" ).mouseover(function() {
		$("#neun_n").css('color', 'red');
		$("#nana_a").css('color', 'red');
		$("#nana_n").css('color', 'red');
		$("#nana_a2").css('color', 'red');
		$( ".nana" ).css('cursor','pointer');
		
		//moving tag
		$(".nana").on('mousemove', function(e){
			$('.qi_tag').css({
				opacity:1,
				left:  e.pageX,
				top:   e.pageY
			});
		});
	});
	
	$( ".nana" ).mouseout(function() {
		$("#neun_n").css('color', 'black');
		$("#nana_a").css('color', 'black');
		$("#nana_n").css('color', 'black');
		$("#nana_a2").css('color', 'black');
		
		//disappear
		$('.qi_tag').css({
			opacity:0
		});
	});
	
	//on click, redirect to the page for japanese
	$( ".nana" ).click(function(){
		document.location = 'http://earthling.blog/';
		// if storing in own directory, use document.location = 'french/french.html';
	});
	
//-------------------------------------------------------------------------
//start functions for "neuf"
	
	$( ".neuf" ).mouseover(function() {
		$(this).css('color', 'red');
		$( ".neuf" ).css('cursor','pointer');
		
		//moving tag
		$(".neuf").on('mousemove', function(e){
			$('.jiu_tag').css({
				opacity:1,
				left:  e.pageX,
				top:   e.pageY
			});
		});
		
	});
	
	$( ".neuf" ).mouseout(function() {
		$(this).css('color', 'black');
		
		//disappear
		$('.jiu_tag').css({
			opacity:0
		});
	});
	
	//on click, redirect to the page for french
	$( ".neuf" ).click(function(){
		document.location = 'http://earthling.blog/'; 
		// if storing in own directory, use document.location = 'french/french.html';
	});
	

});